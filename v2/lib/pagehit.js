const fs = require('fs');
const util = require('util');
const path = require('path');
const httpReferrer = require('./httpreferrer');

const PATH_DATA = './data.txt';

module.exports = class {
    

    // initialize
    constructor() {
        // counter storage
        this.counter = {};
    }

    // increase URL counter
    async count(req) {
        let hash = httpReferrer(req);
        if (!hash) return null;

        // define count default
        this.counter[ hash ] = this.counter[ hash ] || 0;

        // return incremented count
        return ++this.counter[ hash ];
    }

    async countAsync(req) {
        let hash = httpReferrer(req);
        if (!hash) return null;

        // define count default
        let count = 0;

        const existsFunc = file => new Promise(resolve => fs.access(file, fs.constants.F_OK, err => resolve(!err)));
        const isExisted = await existsFunc(PATH_DATA);

        if(isExisted) {
            const readFileFunc =  util.promisify(fs.readFile);
            const content = await readFileFunc(PATH_DATA);
            console.log(`Read Data content: ${content}`);
    
            if(content) {
                const arr = content.toString().split(' ');
                count  = arr.length > 1 ? Number.parseInt(arr[1]) : 0;
            }
        }

        const writereadFileFunc =  util.promisify(fs.writeFile);
        await writereadFileFunc(PATH_DATA, hash + ' ' + ++count);

        return count;
    }
};